package br.ucsal.bes20202.testequalidade.restaurante.enums;

public enum SituacaoMesaEnum {
	LIVRE, OCUPADA;
}
