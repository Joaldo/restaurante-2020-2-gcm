package br.ucsal.bes20202.testequalidade.restaurante.business;

import br.ucsal.bes20202.testequalidade.restaurante.domain.Comanda;
import br.ucsal.bes20202.testequalidade.restaurante.domain.Item;
import br.ucsal.bes20202.testequalidade.restaurante.domain.Mesa;
import br.ucsal.bes20202.testequalidade.restaurante.enums.SituacaoMesaEnum;
import br.ucsal.bes20202.testequalidade.restaurante.exception.ComandaFechadaException;
import br.ucsal.bes20202.testequalidade.restaurante.exception.MesaOcupadaException;
import br.ucsal.bes20202.testequalidade.restaurante.exception.RegistroNaoEncontrado;
import br.ucsal.bes20202.testequalidade.restaurante.persistence.ComandaDao;
import br.ucsal.bes20202.testequalidade.restaurante.persistence.ItemDao;
import br.ucsal.bes20202.testequalidade.restaurante.persistence.MesaDao;
//comentario
public class RestauranteBO {

	private MesaDao mesaDao;
	private ComandaDao comandaDao;
	private ItemDao itemDao;

	public RestauranteBO(MesaDao mesaDao, ComandaDao comandaDao, ItemDao itemDao) {
		this.mesaDao = mesaDao;
		this.comandaDao = comandaDao;
		this.itemDao = itemDao;
	}

	public void abrirComanda(Integer numeroMesa) throws RegistroNaoEncontrado, MesaOcupadaException {
		Mesa mesa = mesaDao.obterPorNumero(numeroMesa);
		if (SituacaoMesaEnum.LIVRE.equals(mesa.getSituacao())) {
			Comanda comanda = new Comanda(new Mesa());
			comandaDao.incluir(comanda);
		} else {
			throw new MesaOcupadaException(numeroMesa);
		}
	}

	public void incluirItemComanda(Integer codigoComanda, Integer codigoItem, Integer qtdItem)
			throws RegistroNaoEncontrado, ComandaFechadaException {
		Comanda comanda = comandaDao.obterPorCodigo(codigoComanda);
		Item item = itemDao.obterPorCodigo(codigoItem);
		comanda.incluirItem(item, qtdItem);
	}

	public Double fecharComanda(Integer codigoComanda) throws RegistroNaoEncontrado {
		Comanda comanda = comandaDao.obterPorCodigo(codigoComanda);
		return comanda.fechar();
	}

}
